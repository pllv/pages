
	I have separated _colors.css_ file to easily change the colors of the web 
	without having to touch the rest of the code. I think it's cleaner this way.


#### CONTAINER COLOR PICKERS
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--pkr-txt		|button text							|(color 2)	|
|--pkr-bg		|button background						|(color 4)	|
|--pkr-bg-hov	|button background on hover & checked	|(color 3)	|
|--pkr-bord		|button border							|(color 1)	|

#### CONTAINER COLOR
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--cont-bg		|container background					|(color 3)	|
|--cont-bord	|container border						|(color 1)	|

#### PAGE
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--txt			|page text								|(color 2)	|
|--bg			|button and page background				|(color 4)	|
|--bord			|border page							|(color 1)	|
|--h1			|h1										|(color 1)	|
|--h2			|h2										|(color 1)	|
|--h3			|h3										|(color 1)	|
|--h4			|h4										|(color 1)	|

#### PAGE BUTTONS
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--btn			|email & code buttons text or symbol	|(color 1)	|
|--btn-bg		|page buttons background				|(color 4)	|
|--btn-bord		|page buttons border					|(color 1)	|

#### DROPDOWNS (DETAILS)
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--det			|details text							|(color 2)	|
|--det-bg		|details background						|(color 3)	|
|--det-bord		|details border							|(color 1)	|

#### TOOLTIPS
|VAR			|ELEMENT								|COLOUR		|
|-				|-										|-			|
|--tool			|tooltips text color					|(color 3)	|
|--tool-bg		|tooltips background					|(color 2)	|
|--tool-bord	|tooltips border						|(color 3)	|


---

---

## Color 1 - Text 1 (Titles and Borders)

#### PAGE
	h1
	h2
	h3
	h4

#### PAGE
- border page

#### PAGE BUTTONS
- email and code buttons text or symbol
- page buttons border

#### CONTAINER COLOR PICKERS
- button border

#### CONTAINER COLOR
- container border

#### DROPDOWNS (DETAILS)
- details border

---

## Color 2 - Text 2 (Page text)

#### CONTAINER COLOR PICKERS
- button text

#### PAGE
- page text

#### DROPDOWNS (DETAILS)
- details text

#### TOOLTIPS
- tooltips background

---

## Color 3 - Background 1 (Background others)

#### CONTAINER COLOR PICKERS
- button background on hover and checked

#### CONTAINER COLOR
- container background

#### DROPDOWNS (DETAILS)
- details background

#### TOOLTIPS
- tooltips text color
- tooltips border

---

## Color 4 - Background 2 (Background page)

#### CONTAINER COLOR PICKERS
- button background

#### PAGE
- button and page background

#### PAGE BUTTONS
- page buttons background

---

# Contrast:
	1 - 3
	2 - 3
	1 - 4
	2 - 4

---
