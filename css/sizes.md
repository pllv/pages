## SIZES

( Yes, I know, too many )

|VAR     |SIZE                              |ELEMENT                                                                                                                        |PROP                               |
|----    |----                              |----                                                                                                                           |----                               |
|size_no |0                                 |details[open] summary                                                                                                          |border                             |
|        |                                  |details h4, <br> details[open] summary                                                                                         |padding                            |
|        |                                  |details[open], <br> #cont_soft, <br> #cont_lang, <br> #cont_soft details[open], <br> #cont_lang details[open]                  |margin-right, <br> margin-left     |
|size_00 |clamp(0.0rem,   2.0vw,   1.0rem)  |header, <br> main, <br> main, <br> footer                                                                                      |padding                            |
|        |                                  |header > p, <br> footer p                                                                                                      |padding-top                        |
|        |                                  |#p_acc                                                                                                                         |padding-bottom                     |
|size_01 |clamp(0.1rem,   1.0vw,   0.5rem)  |.prof-label, <br> .colour-label                                                                                                |margin                             |
|size_02 |clamp(0.1rem,   2.0vw,   0.5rem)  |#block_1 h2::first-letter                                                                                                      |line-height                        |
|        |                                  |.big_button                                                                                                                    |border-radius                      |
|size_03 |clamp(0.1rem,   2.0vw,   1.0rem)  |.prof-container, <br> .colour-container                                                                                        |padding                            |
|size_04 |clamp(0.1rem,   5.0vw,   0.5rem)  |#js, <br> summary .tooltiptext                                                                                                 |padding-top, <br> padding-bottom   |
|        |                                  |details h4                                                                                                                     |padding-bottom                     |
|size_05 |clamp(0.2rem,   1.0vw,   0.5rem)  |.colour-label                                                                                                                  |border-width                       |
|size_06 |clamp(0.2rem,   2.0vw,   0.5rem)  |#block_2 summary                                                                                                               |border-radius                      |
|size_07 |clamp(0.2rem,   2.0vw,   1.0rem)  |h1, <br> h2, <br> h3                                                                                                           |margin-top, <br> margin-bottom     |
|        |                                  |h4                                                                                                                             |padding-top                        |
|size_08 |clamp(0.2rem,   5.0vw,   2.0rem)  |.block                                                                                                                         |padding-bottom                     |
|size_09 |clamp(0.25rem,  2.5vw,   0.75rem) |#cont_soft, <br> #cont_lang, <br> #cont_soft details[open], <br> #cont_lang details[open]                                      |margin-top, <br> margin-bottom     |
|size_10 |clamp(0.3rem,   2.0vw,   1.0rem)  |.big_button                                                                                                                    |margin, <br> padding               |
|size_11 |clamp(0.5rem,   5.0vw,   1.0rem)  |.container_p p                                                                                                                 |margin                             |
|        |                                  |details[open] summary                                                                                                          |margin-bottom, <br> padding-bottom |
|        |                                  |summary                                                                                                                        |padding-top, <br> padding-bottom   |
|        |                                  |#js, <br> summary .tooltiptext                                                                                                 |padding-right, <br> padding-left   |
|        |                                  |details[open] summary:first-of-type::first-letter                                                                              |line-height                        |
|size_12 |clamp(0.5rem,   5.0vw,   1.5rem)  |details[open]                                                                                                                  |margin-top, <br> margin-bottom     |
|        |                                  |details[open], <br> #cont_soft details, <br> #cont_lang details, <br> #cont_soft details[open], <br> #cont_lang details[open]  |padding-top, <br> padding-bottom   |
|size_15 |clamp(1.0rem,   5.0vw,   2.0rem)  |.block                                                                                                                         |margin                             |
|        |                                  |details                                                                                                                        |margin-top, <br> margin-bottom     |
|        |                                  |.big_button svg                                                                                                                |width, <br> height                 |
|        |                                  |#cont_soft, <br> #cont_lang                                                                                                    |column-gap                         |
|        |                                  |details[open], <br> #cont_soft details, <br> #cont_lang details, <br> #cont_soft details[open], <br> #cont_lang details[open]  |padding-right, <br> padding-left   |
|size_16 |clamp(1.0rem,   5.0vw,   3.0rem)  |#block_1 h2                                                                                                                    |padding-left, <br> padding-right   |
|        |                                  |details                                                                                                                        |margin-right, <br> margin-left     |
|size_17 |clamp(1.0rem,   6.0vw,   3.0rem)  |.colour-label                                                                                                                  |height, <br> width                 |
|size_18 |clamp(1.0rem,   7.0vw,   5.0rem)  |#block_1 h3                                                                                                                    |padding-left, <br> padding-right   |
|size_22 |clamp(2.0rem,   7.0vw,   3.0rem)  |#block_3 summary, <br> #block_2 summary:hover                                                                                  |border-radius                      |

### FONTS
|VAR     |SIZE                              |ELEMENT                          |PROP                               |
|----    |----                              |----                             |----                               |
|size_fp |clamp(0.9rem,   5.0vw,   1.2rem)  |p                                |font-size                          |
|size_f4 |clamp(1.0rem,   5.0vw,   1.5rem)  |h4, <br> #p_acc, <br> details    |font-size                          |
|        |                                  |summary                          |padding-right, <br> padding-left   |
|size_f3 |clamp(1.2rem,   5.0vw,   2.0rem)  |h3                               |font-size                          |
|size_f2 |clamp(1.5rem,   5.0vw,   2.5rem)  |h2                               |font-size                          |
|size_f1 |clamp(2.0rem,   5.0vw,   3.0rem)  |h1                               |font-size                          |
|size_fn |clamp(2.3rem,   10.0vw,  4.0rem)  |#name h1                         |font-size                          |
