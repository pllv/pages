# portfolio


### index.html

My simple and readable portfolio.


### index_fun.html

When I started this project I was just wondering what was possible to do with only css.

I know it could be done with far fewer lines using PHP or Javascript; however, CodeBerg does not allow PHP and I refuse to use JS anymore (I have it blocked in my browsers) because it runs on the client, so I consider it invasive.

This website is purely for entertainment.

