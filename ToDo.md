## In progress:

- Links to more works.

- Learn to revert commits :(



## To Do:

- Add pictures to every dropdown (jobs, projects, etc.)

- Add a big svg at the side of each group of dropdowns.

- Courses?

- Width: 643 - 712 px -> Problem.

- Prepare php code for the other project.



## To Do... later:

- Philosophy and license.

- Hobbies (very personal) interests?

- Rick-roll?

- Transitions in color picker buttons at appearing again. How?



## Done:

- Responsive (!important)

- LC 133395.

- Rearrange software.

- Load html text from css file (everything).

- Javascript is invasive (in a tooltip).

- Fix the difference between open and closed dropdown margins and paddings. (...more or less)

- No shadows.

- Color buttons at the end.

- Separate colors_style from style.

- Interchange border and background colours in training.

- ~~Hide and show both colors and professions.~~

- Checkers for visibility of professions.

- Add icons to profession selectors.

- Be polite.

- Add background circles to every experience element.

- Re-arrange _Others_ (courses?) and _Others_ (degrees and diplomas).

- Link to works ~~, maybe in another project... or repository/web?~~ !important

- Second web. An accessible version.

- Border-color changed.
