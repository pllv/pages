The style of this web is inspired by (I mean by A LOT; sorry, guys) Canonical's web.

## Colors:

Foreground (text) Color (#ddd) to Background Color (#262626)

Contrast Ratio 11.14:1 		(WCAG AAA: Pass)

The WCAG guidelines recommend the following contrast ratios:

| Type of content | Min. ratio (AA rating) | Enhanced ratio (AAA rating)|
| --- | --- | --- |
| Body text | 4.5 : 1 | 7 : 1 |


[WebAIM](https://webaim.org/resources/contrastchecker/?fcolor=DDDDDD&bcolor=262626)

[Mozilla Color Contrast](https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Perceivable/Color_contrast?utm_source=devtools&utm_medium=a11y-panel-checks-color-contrast)

